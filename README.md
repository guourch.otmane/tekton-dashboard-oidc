# tekton-dashboard-oidc

This repo contains steps to secure your tekton dashboard via OIDC Okta.

## Getting started

To make it easy for you to get started with Tekton Dashboard with OIDC, here's a list of recommended next steps.

1. Create a Kubernetes secret with kubectl:

`kubectl create secret generic tekton-dashboard-auth--from-literal=username=YOUR_CLIENT_ID --from-literal=password=YOUR_CLIENT_SECRET`

2. Edit the manifest above with your Okta configuration.

3. Apply the manifest : 

`kubect apply -f tekton-dashboard-oidc.yaml`
